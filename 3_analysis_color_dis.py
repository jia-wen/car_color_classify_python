import cv2
import numpy as np
from matplotlib import pyplot as plt
import os


def calc_hist(img, hsv_all):
    hsv_dict = dict()
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    s_h, s_s, s_v = cv2.split(hsv)
    cv2.equalizeHist(s_v, s_v)
    hsv = cv2.merge([s_h, s_s, s_v])

    # bg_mask = cv2.inRange(hsv, np.array([5, 0, 44]), np.array([79, 221, 238]))  # bg
    bg_mask = cv2.inRange(hsv, np.array([0, 14, 30]), np.array([70, 112, 213]))  # new_bg
    fg_mask = cv2.threshold(~bg_mask, 127, 255, cv2.THRESH_BINARY)[1]

    fg_mask = cv2.erode(fg_mask, None, iterations=3)
    fg_mask = cv2.dilate(fg_mask, None, iterations=2)
    hsv_fg = cv2.add(hsv, np.zeros(np.shape(hsv), dtype=np.uint8), mask=fg_mask)

    fg_range_mask = cv2.inRange(hsv_fg, np.array([0, 0, 25]), np.array([180, 255, 229]))
    fg_range_mask = cv2.threshold(fg_range_mask, 127, 255, cv2.THRESH_BINARY)[1]
    hsv_fg_in_range = cv2.add(hsv, np.zeros(np.shape(hsv), dtype=np.uint8), mask=fg_range_mask)

    # hsv_fg = cv2.cvtColor(img_fg, cv2.COLOR_BGR2HSV)
    # cv2.imshow("img", img)
    # cv2.imshow("mask", fg_range_mask)
    # cv2.imshow("hsv", hsv)
    # cv2.imshow("hsv_fg", hsv_fg)
    # cv2.imshow("hsv_fg_in_range", hsv_fg_in_range)
    # cv2.waitKey()
    # cv2.destroyAllWindows()

    for channel, channel_name in enumerate(('h', 's', 'v')):
        if channel == 0:
            hsv_dict[channel_name] = cv2.calcHist([hsv_fg_in_range], [channel, ], None, [180], [1, 180])
        else:
            hsv_dict[channel_name] = cv2.calcHist([hsv_fg_in_range], [channel, ], None, [255], [1, 255])

    # hsv_show(hsv_dict, 1)

    for key, value in hsv_dict.items():  # 这里想把一个dict的每个key里的narray（value）里的每个元素相加
        if key in hsv_all.keys():
            hsv_all[key] += value
        else:
            hsv_all[key] = value
    return hsv_all


def hsv_show(hist, imgs_num):
    color = ('h', 's', 'v')
    plt.figure()
    for channel, channel_name in enumerate(color):
        plt.subplot(3, 1, channel + 1)
        plt.plot(hist[channel_name]/imgs_num)
    plt.show()


# 保存每张图片hsv
# 把一张图的每个点的hsv都画出来，然后纵坐标是一个像素值的值，全部图相加再除以图片总数
# color_dir = "./crop_images/3_manual_select_from_detect/red2"

test_color = ""
color_dir = "/home/wpf/car_color_classify/crop_images/4_manual_classify/red2"

if __name__ == '__main__':
    # color = dict()
    hsv_all = {"h": np.zeros((180, 1)), "s": np.zeros((255, 1)), "v": np.zeros((255, 1))}
    select_list = os.listdir(color_dir)
    for filename in select_list:
        image_path = os.path.join(color_dir, filename)
        # try:
        img = cv2.imread(image_path)
        # img = cv2.imread('./crop_images/select/color/blue/239_rotate_0001.jpg')
        car_color = calc_hist(img, hsv_all)
        # except:
        #     continue

    hsv_show(hsv_all, len(select_list))
    # hsv_show(hsv_all, 1)
