import cv2
import numpy as np
import colorList
import os


def get_color(frame):
    # print('go in get_color')
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    s_h, s_s, s_v = cv2.split(hsv)
    cv2.equalizeHist(s_v, s_v)
    hsv = cv2.merge([s_h, s_s, s_v])

    maxsum = -100
    color = None
    color_dict = colorList.getColorList()
    result = dict()
    for d in color_dict:
        if d == 'white1' or d == 'red1' or d == 'blue1':
            # bg_mask = cv2.inRange(hsv, np.array([5, 0, 44]), np.array([79, 221, 238]))  # bg
            bg_mask = cv2.inRange(hsv, np.array([0, 14, 30]), np.array([70, 112, 213]))  # new_bg
            fg_mask = cv2.threshold(~bg_mask, 127, 255, cv2.THRESH_BINARY)[1]
            fg_mask = cv2.erode(fg_mask, None, iterations=3)
            fg_mask = cv2.dilate(fg_mask, None, iterations=2)
            hsv_fg = cv2.add(hsv, np.zeros(np.shape(hsv), dtype=np.uint8), mask=fg_mask)

            fg_range_mask = cv2.inRange(hsv_fg, np.array([0, 0, 25]), np.array([180, 255, 229]))
            fg_range_mask = cv2.threshold(fg_range_mask, 127, 255, cv2.THRESH_BINARY)[1]
            hsv_fg_in_range = cv2.add(hsv, np.zeros(np.shape(hsv), dtype=np.uint8), mask=fg_range_mask)

            fg_color_mask = cv2.inRange(hsv_fg_in_range, color_dict[d][0], color_dict[d][1])
            fg_color_mask = cv2.threshold(fg_color_mask, 127, 255, cv2.THRESH_BINARY)[1]

            # # hsv_fg = cv2.add(frame, np.zeros(np.shape(frame), dtype=np.uint8), mask=fg_mask)
            # cv2.imshow("img", frame)
            # cv2.imshow("fg_mask", fg_mask)
            # cv2.imshow("final_mask", fg_color_mask)
            # cv2.waitKey()
            # cv2.destroyAllWindows()

            sum = 0
            sum = np.sum(fg_color_mask)
            result[d] = sum
            if sum > maxsum:
                maxsum = sum
                color = d
    return color, result


test_color_list = True
#################
#  try colorList

# file_path = {"blue1": "./crop_images/4_manual_classify/blue",
#              "red1": "./crop_images/4_manual_classify/red2"}

file_path = {  # "bg1": "./crop_images/4_manual_classify/bg1",
               # "bg2": "./crop_images/4_manual_classify/bg2",
             "blue1": "./crop_images/4_manual_classify/blue",
             "white1": "./crop_images/4_manual_classify/white",
             "red1": "./crop_images/4_manual_classify/red2"}

save_path = './crop_images/4_manual_classify/5_result_of_test'
#################
#  test
# file_path = {"test": "./crop_images/select"}
# save_path = './crop_images/6_test_crop'

if __name__ == '__main__':
    for color, select_dir in file_path.items():
        select_list = os.listdir(select_dir)
        for filename in select_list:
            image_path = os.path.join(select_dir, filename)
            # try:
            frame = cv2.imread(image_path)
            car_color, result = get_color(frame)
            # except:
            #     continue

            # save_path_in_color = os.path.join("{}/{}".format(save_path, color))
            if not test_color_list:
                save_path_in_color = os.path.join(save_path, car_color)
                if not os.path.exists(save_path_in_color):
                    os.makedirs(save_path_in_color)
            else:
                if car_color == color:
                    save_path_in_color = os.path.join(save_path, color, "right")
                else:
                    save_path_in_color = os.path.join(save_path, color, "fault_as_{}".format(car_color))
                if not os.path.exists(save_path_in_color):
                    os.makedirs(save_path_in_color)
            cv2.imwrite(os.path.join(save_path_in_color,
                                     "{}_{}_{}_{}".format(result["red1"]/255, result["blue1"]/255, result["white1"]/255, filename.split('/')[-1])), frame)
