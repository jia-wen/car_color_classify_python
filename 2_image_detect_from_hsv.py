import cv2
import numpy as np
import colorList
import os

# filename = './crop_images/select/1892_rotate_0001.jpg'
save_path = './crop_images/select/color'
select_dir = './crop_images/select'
# cv2.namedWindow('aaa', cv2.WINDOW_NORMAL)

# 处理图片
def get_color(frame):
    # print('go in get_color')
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    maxsum = -100
    color = None
    color_dict = colorList.getColorList()
    for d in color_dict:
        # if d == 'green' or d == 'purple' or d == 'orange' or d == 'black':
        #     continue
        if d == 'white' or d == 'red' or d == 'red2' or d == 'blue':
            mask = cv2.inRange(hsv, color_dict[d][0], color_dict[d][1])
            # cv2.imwrite(d + '.jpg', mask)
            binary = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)[1]
            binary = cv2.dilate(binary, None, iterations=2)
            sum = 0
            sum = np.sum(binary)

            if sum > maxsum:
                maxsum = sum
                color = d


    return color


if __name__ == '__main__':
    color = dict()
    select_list = os.listdir(select_dir)
    for filename in select_list:
        image_path = os.path.join(select_dir, filename)
        try:
            frame = cv2.imread(image_path)
            car_color = get_color(frame)
        except:
            continue

        save_path_in_color = os.path.join("{}/{}".format(save_path, car_color))
        if not os.path.exists(save_path_in_color):
            os.mkdir(save_path_in_color)
        cv2.imwrite(os.path.join(save_path_in_color, filename.split('/')[-1]), frame)
