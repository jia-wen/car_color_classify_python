import os
import cv2
import re

pattens = ['name', 'xmin', 'ymin', 'xmax', 'ymax']


def get_annotations(xml_path):
    bbox = []
    with open(xml_path, 'r') as f:
        text = f.read().replace('\n', 'return')
        p1 = re.compile(r'(?<=<object>)(.*?)(?=</object>)')
        result = p1.findall(text)
        for obj in result:
            tmp = []
            for patten in pattens:
                p = re.compile(r'(?<=<{}>)(.*?)(?=</{}>)'.format(patten, patten))
                if patten == 'name':
                    tmp.append(p.findall(obj)[0])
                else:
                    tmp.append(int(float(p.findall(obj)[0])))
            bbox.append(tmp)
    return bbox


def save_viz_image(image_path, xml_path, save_path):
    bbox = get_annotations(xml_path)
    image = cv2.imread(image_path)
    if not os.path.exists(save_path):
        os.mkdir(save_path)
    for info in bbox:
        # cv2.rectangle(image, (info[1], info[2]), (info[3], info[4]), (255, 0, 0), thickness=2)
        if info[3] - info[1] == 0 or info[4] - info[2] == 0:
            continue
        try:
            crop_img = image[info[1]:info[3], info[2]:info[4]]
        except:
            continue
        cv2.imwrite(os.path.join(save_path, image_path.split('/')[-1]), crop_img)

    # cv2.imwrite(os.path.join(save_path, image_path.split('/')[-1]), image)


if __name__ == '__main__':
    image_dir = 'JPEGImages'
    xml_dir = 'Annotations'
    save_dir = 'crop_images'
    xml_list = os.listdir(xml_dir)
    count = 0
    amount = len(xml_list)
    for i in xml_list:
        count = count + 1
        xml_path = os.path.join(xml_dir, i)
        image_path = os.path.join(image_dir, i.replace('.xml', '.jpg'))
        save_viz_image(image_path, xml_path, save_dir)
        if 10 * count/amount % 1 == 0:
            print("{}%".format(count*100.0/amount))