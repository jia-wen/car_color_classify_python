import numpy as np
import collections


def getColorList():
    dict = collections.defaultdict(list)
    # [106  20  29]
    # [119 150 180]

    # lower_white = np.array([110, 100, 69])
    # upper_white = np.array([120, 140, 254])
    lower_white = np.array([110, 64, 69])
    upper_white = np.array([120, 140, 254])
    # lower_white = np.array([105, 64, 69])
    # upper_white = np.array([130, 140, 254])
    color_list = []
    color_list.append(lower_white)
    color_list.append(upper_white)
    dict['white1'] = color_list

    lower_red = np.array([141, 52, 45])
    upper_red = np.array([255, 151, 254])
    # lower_red = np.array([0, 100, 50])
    # upper_red = np.array([25, 170, 254])
    color_list = []
    color_list.append(lower_red)
    color_list.append(upper_red)
    dict['red1'] = color_list

    # lower_blue = np.array([86, 75, 140])  # range
    # upper_blue = np.array([115, 123, 210])
    lower_blue = np.array([86, 90, 53])  # filter
    upper_blue = np.array([120, 170, 253])
    # lower_blue = np.array([80, 90, 53])
    # upper_blue = np.array([124, 170, 253])
    color_list = []
    color_list.append(lower_blue)
    color_list.append(upper_blue)
    dict['blue1'] = color_list

    # # bg1
    # lower_black = np.array([0, 0, 100])
    # upper_black = np.array([50, 100, 255])
    # color_list = []
    # color_list.append(lower_black)
    # color_list.append(upper_black)
    # dict['bg1'] = color_list
    #
    # lower_black = np.array([0, 0, 0])
    # upper_black = np.array([50, 50, 100])
    # color_list = []
    # color_list.append(lower_black)
    # color_list.append(upper_black)
    # dict['bg2'] = color_list

    return dict


if __name__ == '__main__':
    color_dict = getColorList()
    print(color_dict)

    num = len(color_dict)
    print('num=', num)

    for d in color_dict:
        print('key=', d)
        print('value=', color_dict[d][1])